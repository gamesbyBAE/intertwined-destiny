#### SampleScene.unity 
-> Above scene is reference scene for admob, MoreGames, Google Play games and Game Analytics

#### Prefab 
-> "NagSDK" is the prefab which contains the admob, MoreGames, Google Play games and Game Analytics child in that which has respective scripts



#### AdManager Prefab is responsible for google ads  ( Latest Version )

1. Add Keys in Inspector with respective fields :- Interstitial, Video and Banner

// How to show the Ads :-
2. Show Interstitial :- 
GoogleAdMobController.Instance.ShowInterstitialAd();

3. Show BannerAds:-
options :-
    a. Normal banner will show 
        GoogleAdMobController.Instance.ShowBanner( );

    b. Base Banner with AdPosition ShowBanner 
        GoogleAdMobController.Instance.ShowBanner( AdPosition adPos );

    c. With Position where to show the banner in screen with reference to gameobject
        GoogleAdMobController.Instance.ShowBanner( gameobject );

4. Show Rewarded ads:-
        GoogleAdMobController.Instance.ShowRewardedAd( () => { Give your reward function } );



#### Google play games login 

1. Add the config in the google play games // Keys for all can be found in GPGSIds.cs

2. GPGamesController.cs has all the base functionality like 

    a. IncrementAchievement()
    b. UnlockAchievement()
    c. UnlockProgressBasedAchievements()

    //Post the score to Leaderboard
    d. PostToLeaderboard( int score );
    
    // Will show the default leaderboard
    e. ShowLeaderboardUI()

    // Will show the defautlt Achievement
    f. ShowAchievementsUI()

#### GAManager for Game Analytics 

1. Assets/ Resources/ GameAnalytics/ settings
This above path where we can set the Game Key and Secrete Key with respective platform

YOU CAN DIRECTLY FOUND THAT IN ABOVE TAB :- windows -> GameAnalytics -> Select Settings

NOTE:- Only init is their in GAManager
how to call any event :- Reference is given in GAManager   

Check GA scripts For different events :- GameAnalytics/Plugins/Scripts/Events

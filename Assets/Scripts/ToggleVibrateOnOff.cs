﻿using UnityEngine;
using UnityEngine.UI;
public class ToggleVibrateOnOff : MonoBehaviour
{
    [SerializeField]
    GameManager gameManager;

    private void Start()
    {
        if(this.gameManager.VibrateDevice)
        {
            GetComponent<Toggle>().isOn = false;
        }
        else
        {
            GetComponent<Toggle>().isOn = true;
        }
    }
    public void ToggleVibration(bool boolVal)
    {
        if (boolVal)
        {
            this.gameManager.VibrateDevice = false;
        }
        else
        {
            this.gameManager.VibrateDevice = true;
        }
    }
    
}

﻿using UnityEngine;

public class ChangeInputType : MonoBehaviour
{
    [SerializeField] GameManager gameManager;

    public void UpdateInputTypeValue(int itv)
    {
        this.gameManager.InputType = itv;
    }
}

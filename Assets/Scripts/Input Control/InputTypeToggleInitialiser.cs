﻿using UnityEngine;
using UnityEngine.UI;

public class InputTypeToggleInitialiser : MonoBehaviour
{
    [SerializeField] GameManager gameManager;
    [SerializeField] Toggle[] inputTypeOptions;
    void Awake()
    {
        inputTypeOptions[this.gameManager.InputType - 1].isOn = true;
    }
}

﻿using UnityEngine;

public class TwoFingerInput : MonoBehaviour
{
    PlayerController pc;
    Touch touch;
    int screenCentre;

    [SerializeField] GameObject twoFingersInstructions;
    void Start()
    {
        pc = GetComponent<PlayerController>();
        screenCentre = Screen.width / 2;
        if (twoFingersInstructions)
        {
            twoFingersInstructions.SetActive(true);
        }
    }

    void Update()
    {
        if (Input.touchCount > 0)
        {
            TouchTwoFingerInput();
        }

        MouseTwoButtonInput();
    }

    void TouchTwoFingerInput()
    {
        touch = Input.GetTouch(0);
        if (touch.phase == TouchPhase.Ended)
        {
            pc.SwitchValue = 0;
        }
        else
        {
            if (touch.position.x > screenCentre)
            {
                pc.SwitchValue = 1;
            }
            else if (touch.position.x < screenCentre)
            {
                pc.SwitchValue = -1;
            }
        }
    }
    void MouseTwoButtonInput()
    {
        if (Input.GetMouseButtonDown(1))
        {
            pc.SwitchValue = 1;
        }
        else if (Input.GetMouseButtonDown(0))
        {
            pc.SwitchValue = -1;
        }
        else if (Input.GetMouseButtonUp(0) || Input.GetMouseButtonUp(1))
        {
            pc.SwitchValue = 0;
        }
    }
}

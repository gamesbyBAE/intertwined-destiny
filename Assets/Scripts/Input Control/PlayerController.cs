﻿using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] Rigidbody2D lRb = null;
    [SerializeField] CircleCollider2D leftCollider;

    [SerializeField] Rigidbody2D rRb = null;
    [SerializeField] CircleCollider2D rightCollider;
    
    [SerializeField] GameManager gameManager;

    [SerializeField] Vector2 upDirection = Vector2.zero;
    [SerializeField] float upForceX = 0;
    [SerializeField] float upForceValue = 0;

    [SerializeField] Vector2 downDirection = Vector2.zero;
    [SerializeField] float downForceX = 0;
    [SerializeField] float downForceValue = 0;

    [SerializeField] Vector2 bothDownDirection = Vector2.zero;
    Vector3 positionChange;
    public int SwitchValue { get; set; }
    [HideInInspector] public int oldValue = 1;


    void Start()
    {
        //rRb.GetComponent<SpriteRenderer>().color = this.gameManager.BallColor;
        //lRb.GetComponent<SpriteRenderer>().color = this.gameManager.BallColor;

        upDirection = new Vector2(upForceX, 1);
        downDirection = new Vector2(downForceX, -1);
        SwitchValue = 0;
        positionChange = Vector3.zero;

        
        // Stopping "Game Over" audio if still playing after reloading scene since SoundManager has DontDestroyOnLoad()
        if (SoundManager.instance != null)
        {
            AudioSource gameOver = SoundManager.instance.GetAudioSource(SoundClip.SoundListEnum.GameOver);
            if (gameOver.isPlaying)
            {
                gameOver.Stop();
            }
        }


        // Get value of InputControlType, if doesn't exist then create and set the value 1
        // 1: signifies One Finger Input
        // 2: signifies Two Finger Input

        if (gameManager.InputType == 1)
        {
            gameObject.GetComponent<OneFingerInput>().enabled = true;
            gameObject.GetComponent<TwoFingerInput>().enabled = false;
        }
        else if (gameManager.InputType == 2)
        {
            gameObject.GetComponent<OneFingerInput>().enabled = false;
            gameObject.GetComponent<TwoFingerInput>().enabled = true;
        }
    }


    void Update()
    {
        //  Value of SwitchValue decides which circle moves at which angle and at what speed.
        //  1: signifies moving the right circle in top-right direction
        // -1: signifies moving the left circle in top-left direction
        //  0: signifies both the circles falling down
        // 69: signifies halt, stop wherever the circles are right now when Game Overs

        switch (SwitchValue)
        {
            case 1:
                SpriteRotate(rRb.gameObject, -25);
                positionChange = MoveBallUp(upForceX);
                rRb.MovePosition(rRb.transform.position + positionChange);
                break;

            case -1:
                SpriteRotate(lRb.gameObject, 25);
                positionChange = MoveBallUp(-upForceX);
                lRb.MovePosition(lRb.transform.position + positionChange);
                break;

            case 0:
                if (rRb.transform.rotation.z != 0 || lRb.transform.rotation.z != 0)
                {
                    LeanTween.rotateZ(rRb.gameObject, 0, 0.5f);
                    LeanTween.rotateZ(lRb.gameObject, 0, 0.5f);
                }

                positionChange = bothDownDirection * downForceValue * Time.deltaTime;
                rRb.transform.position = rRb.transform.position + positionChange;
                lRb.transform.position = lRb.transform.position + positionChange;
                break;

            case 69:
                break;
        }
    }

    Vector2 MoveBallUp(float _upForceX)
    {
        upDirection.x = _upForceX;
        return upDirection * upForceValue * Time.deltaTime;
    }

    void SpriteRotate(GameObject go, float angle)
    {
        if (LeanTween.isTweening(go))
        {
            LeanTween.cancel(go);
        }
        LeanTween.rotateZ(go, angle, 0.2f);
    }

/*    Vector2 MoveBallDown(float _downForceX)
    {
        downDirection.x = _downForceX;
        return downDirection * downForceValue * Time.deltaTime;
    }*/

    public void StopMotion()
    {
        gameObject.GetComponent<OneFingerInput>().enabled = false;
        gameObject.GetComponent<TwoFingerInput>().enabled = false;
        leftCollider.enabled = false;
        rightCollider.enabled = false;
        SwitchValue = 69;
    }
}

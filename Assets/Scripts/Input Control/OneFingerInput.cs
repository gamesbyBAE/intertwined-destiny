﻿using UnityEngine;

public class OneFingerInput : MonoBehaviour
{
    [SerializeField] GameObject oneFingerInstructions;
    PlayerController pc;
    Touch touch;
    void Start()
    {
        pc = GetComponent<PlayerController>();
        if (oneFingerInstructions)
        {
            oneFingerInstructions.SetActive(true);
        }   
    }
    void Update()
    {
        if (Input.touchCount > 0)
        {
            TouchOneFingerInput();
        }

        MouseOneButtonInput();
    }

    void TouchOneFingerInput()
    {
        touch = Input.GetTouch(0);
        if (touch.phase == TouchPhase.Ended)
        {
            pc.SwitchValue = 0;
            pc.oldValue = -pc.oldValue;
        }
        else
        {
            pc.SwitchValue = pc.oldValue;
        }
    }

    void MouseOneButtonInput()
    {
        if (Input.GetMouseButtonUp(1))
        {
            pc.SwitchValue = 0;
            pc.oldValue = -pc.oldValue;
        }
        else if (Input.GetMouseButton(1))
        {
            pc.SwitchValue = pc.oldValue;
        }
    }
}

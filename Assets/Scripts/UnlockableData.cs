﻿using UnityEngine;
[CreateAssetMenu(fileName = "New Unlockable Data", menuName = "Unlockable Data", order=54)]
public class UnlockableData : ScriptableObject
{
    [SerializeField] int serialNo;

    [SerializeField] Color obstacleColor;

    [SerializeField] Color backgroundColor;

    [SerializeField] GameObject newObstacle;

    [SerializeField] int newObstacleCount;

    [SerializeField] float verticalMin, verticalMax;

    [SerializeField] int pointsToFinishLevel;

    #region Setters & Getters
    public Color ObstacleColor { get { return obstacleColor; } }
    public Color BackgroundColor { get { return backgroundColor; } }
    public GameObject NewObstacle { get { return newObstacle; } }
    public int NewObstacleCount { get { return newObstacleCount; } }
    public float VerticalMin { get { return verticalMin; } }
    public float VerticalMax { get { return verticalMax; } }
    public int PointsToFinishLevel { get { return pointsToFinishLevel; } }
    #endregion

}

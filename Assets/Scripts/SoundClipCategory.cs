﻿using UnityEngine;
[CreateAssetMenu(fileName ="New Sound Clips Category", menuName = "Sound/Sound Clip Category",order = 61)]
public class SoundClipCategory : ScriptableObject
{
    [SerializeField]
    SoundClip[] soundClips;

    public SoundClip[] SoundClips
    {
        get
        {
            return soundClips;
        }
    }
}

﻿using UnityEngine;

public class ChangeObstacleColor : MonoBehaviour
{
    [SerializeField] GameManager gameManager;
    void Start()
    {
        ChangeColor();
    }

    void ChangeColor()
    {
        Color newColor = this.gameManager.ObstacleColor;

        if (this.gameObject.GetComponent<SpriteRenderer>() != null && this.gameObject.GetComponent<SpriteRenderer>().color != newColor)
        {
            this.gameObject.GetComponent<SpriteRenderer>().color = newColor;
        }
        else if (this.gameObject.GetComponent<SpriteRenderer>() == null)
        {
            for (int k = 0; k < this.gameObject.transform.childCount; k++)
            {
                if (this.gameObject.transform.GetChild(k).GetComponent<SpriteRenderer>().color != newColor)
                {
                    this.gameObject.transform.GetChild(k).GetComponent<SpriteRenderer>().color = newColor;
                }
            }
        }
    }
}

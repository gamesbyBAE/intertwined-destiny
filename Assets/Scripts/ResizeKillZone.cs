﻿using UnityEngine;

public class ResizeKillZone : MonoBehaviour
{
    Vector2 screenBounds;

    EdgeCollider2D edgeCol;
    Vector2[] edgeColPoints;

    [SerializeField] Camera mainCam;
    [SerializeField] float xOffset, yBottomOffset;
    void Start()
    {
        //Getting the top-right coordinate according to screen resolution
        screenBounds = mainCam.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, mainCam.nearClipPlane));
        screenBounds.y -= mainCam.transform.position.y;

        edgeCol = GetComponent<EdgeCollider2D>();
        edgeColPoints = new Vector2[4];

        // Changing the points of edge collider to fit the screen resolution along 
        // with adding some extra space in the sides as well as the bottom.

        //Top-Right point
        edgeColPoints[3].x = screenBounds.x + xOffset;
        edgeColPoints[3].y = screenBounds.y;

        //Top-Left point
        edgeColPoints[0].x = -edgeColPoints[3].x;
        edgeColPoints[0].y = screenBounds.y;

        //Bottom-Right point
        edgeColPoints[2].x = screenBounds.x + xOffset;
        edgeColPoints[2].y = -(screenBounds.y + yBottomOffset);

        //Bottom-Left point
        edgeColPoints[1].x = -edgeColPoints[2].x;
        edgeColPoints[1].y = edgeColPoints[2].y;

        edgeCol.points = edgeColPoints;
    }
}

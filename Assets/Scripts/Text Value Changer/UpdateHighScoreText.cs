﻿using UnityEngine;
using TMPro;

public class UpdateHighScoreText : MonoBehaviour
{
    [SerializeField] GameManager gameManager;
    [SerializeField] GameEvent highScore;
    TextMeshProUGUI highScoreText;

    private void OnEnable()
    {
        if (this.gameManager.ScoreValue > this.gameManager.HighScore)
        {
            gameManager.HighScore = gameManager.ScoreValue;
            LeanTween.delayedCall(0.35f, ()=> highScore.Raise());
        }
        highScoreText = GetComponent<TextMeshProUGUI>();
        highScoreText.text = this.gameManager.HighScore.ToString();
    }
}

﻿using UnityEngine;
using TMPro;

public class ScoreTextUpdater : MonoBehaviour
{
    [SerializeField] GameManager gameManager;

    TextMeshProUGUI scoreText;
    private void OnEnable()
    {
        scoreText = GetComponent<TextMeshProUGUI>();
        scoreText.text = this.gameManager.ScoreValue.ToString();
    }

    public void UpdateScore()
    {
        if (this.gameManager.InfiniteMode)
        {
            scoreText.text = this.gameManager.ScoreValue.ToString();
        }
    }
}

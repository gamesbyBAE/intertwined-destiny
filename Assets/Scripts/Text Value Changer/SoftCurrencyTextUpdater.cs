﻿using UnityEngine;
using TMPro;

public class SoftCurrencyTextUpdater : MonoBehaviour
{
    [SerializeField] GameManager gameManager;

    TextMeshProUGUI myText;
    private void Start()
    {
        myText = GetComponent<TextMeshProUGUI>();
        myText.text = this.gameManager.SoftCurrency.ToString();
    }

    public void UpdateSoftCurrency(bool gameOver)
    {
        if (this.gameManager.InfiniteMode)
        {
            // When Continue Time limit is over or after Score Multiplying Ad.
            if (!gameOver)
            {
                TweenValue(this.gameManager.ScoreValue);
            }
            // When the game is over after alreay Continuing once.
            else if (gameOver && this.gameManager.Continued)
            {
                TweenValue(this.gameManager.ScoreValue);
            }
        }
    }

    public void AddSoftCurrency(int amount)
    {
        TweenValue(amount);
    }

    public void SubtractSoftCurrency(int amount)
    {
        TweenValue(-amount);
    }

    void TweenValue(int amountToAdd)
    {
        var finalValue = amountToAdd + this.gameManager.SoftCurrency;
        LeanTween.value(this.gameManager.SoftCurrency, finalValue, 0.5f).setDelay(0.5f).setOnUpdate((float val) =>
        {
            int wholeVal = (int)val;
            myText.text = wholeVal.ToString();
        });
        this.gameManager.SoftCurrency = finalValue;
    }
}

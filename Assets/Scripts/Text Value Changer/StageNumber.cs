﻿using UnityEngine;
using TMPro;

public class StageNumber : MonoBehaviour
{
    [SerializeField] GameManager gameManager;
    [SerializeField] bool currentStage, nextStage;
    void Start()
    {
        var number = this.gameManager.ThemeID;
        if (currentStage)
        {
            GetComponent<TextMeshProUGUI>().text = $"{number + 1}";
        }
        else if (nextStage && (number + 2 <= this.gameManager.UnlockableDatas.Length))
        {
            GetComponent<TextMeshProUGUI>().text = $"{number + 2}";
        }
        else if (nextStage && (number + 2 > this.gameManager.UnlockableDatas.Length))
        {
            GetComponent<TextMeshProUGUI>().text = "F";
        }
    }
}

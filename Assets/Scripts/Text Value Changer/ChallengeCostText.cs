﻿using UnityEngine;
using TMPro;
public class ChallengeCostText : MonoBehaviour
{
    [SerializeField] GameManager gameManager;
    TextMeshProUGUI myText;
    void Start()
    {
        myText = GetComponent<TextMeshProUGUI>();
        myText.text = this.gameManager.ChallengeCost.ToString();
    }
}

﻿using TMPro;
using UnityEngine;

public class ScoreGUI : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI scoreText;
    [SerializeField] TextMeshProUGUI softCurrencyText;
    [SerializeField] TextMeshProUGUI highScoreText;

    [SerializeField] GameManager gameManager;
    private void Start()
    {
        if (this.gameManager.InfiniteMode)
        {
            scoreText.text = gameManager.ScoreValue.ToString();
            softCurrencyText.text = gameManager.SoftCurrency.ToString();
            highScoreText.text = gameManager.HighScore.ToString();
        }
        else
        {
            softCurrencyText.text = gameManager.SoftCurrency.ToString();
        }
    }

    public void UpdateScore()
    {
        if (this.gameManager.InfiniteMode)
        {
            scoreText.text = gameManager.ScoreValue.ToString();
        }
    }
}

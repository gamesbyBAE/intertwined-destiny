﻿using UnityEngine;

public class ActivateDeactivateObject : MonoBehaviour
{
    [SerializeField] GameManager gameManager;
    [SerializeField] bool deactivateOnStart;

    GameObject[] children;
    private void Start()
    {
        children = new GameObject[this.gameObject.transform.childCount];
        for (int i = 0; i < children.Length; i++)
        {
            children[i] = this.gameObject.transform.GetChild(i).gameObject;
            if (deactivateOnStart && !this.gameManager.WatchedAd)
            {
                children[i].SetActive(false);
            }
        }

        
    }
    public void ActivateObject()
    {
        int count = this.gameObject.transform.childCount;
        //Instance.SetCanvasActive(true);
        for (int i = 0; i < children.Length; i++)
        {
            children[i].SetActive(true);
        }
    }
    public void DeactivateObject()
    {
        this.gameObject.SetActive(false);
    }
}

﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ObstaclePoolSpawn : MonoBehaviour
{
    [System.Serializable]
    public class Obstacles
    {
        public UnlockableData unlockableData;
        public Queue<GameObject> obstacleQueue;

        public Obstacles(UnlockableData unlockableData)
        {
            this.unlockableData = unlockableData;
            obstacleQueue = new Queue<GameObject>();
        }
    }

    Obstacles[] superArray;

    [SerializeField]
    GameManager gameManager;

    float verticalMin, verticalMax, verticalValue;

    [SerializeField]
    float horizontalMax;
    float horizontalMin, horizontalValue;
    float lastVerticalValue;

    [SerializeField]
    GameObject levelFinish;
    
    int maxCount, spawnedObstacleCount;
    int[] shuffleIndex;

    void Awake()
    {
        //ToggleNAGames.Instance.SetCanvasActive(false);

        horizontalMin = -horizontalMax;
        lastVerticalValue = transform.position.y;

        if (this.gameManager.InfiniteMode)
        {
            verticalMin = this.gameManager.UnlockableDatas[0].VerticalMin;
            verticalMax = this.gameManager.UnlockableDatas[0].VerticalMax;

            maxCount = this.gameManager.UnlockableDatas.Length;
            // Randomly selecting non-repeating Background & Obstacle colors.
            if (!this.gameManager.Continued)
            {
                int currentIndex = this.gameManager.UsedThemeID;
                var range = Enumerable.Range(0, maxCount).Where(i => i != currentIndex);
                int randomIndex = range.ElementAt(Random.Range(0, maxCount-1));

                this.gameManager.UsedThemeID = randomIndex;
                this.gameManager.BackgroundColor = this.gameManager.UnlockableDatas[randomIndex].BackgroundColor;
                this.gameManager.ObstacleColor = this.gameManager.UnlockableDatas[randomIndex].ObstacleColor;
            }

            #region Shuffle Index
            // New array to hold indices and shuffling them, so that in each run 
            // obstacles do not spawn in same sequence.
            shuffleIndex = new int[maxCount];
            for (int i = 0; i < maxCount; i++)
            {
                shuffleIndex[i] = i;
            }
            
            // Fisher Yates Shuffle
            for (int t = 1; t < maxCount; t++)
            {
                int r = Random.Range(t, maxCount);
                int tmp = shuffleIndex[r];
                shuffleIndex[r] = shuffleIndex[t];
                shuffleIndex[t] = tmp;
            }
            #endregion
        }
        else if (!this.gameManager.InfiniteMode)
        {
            verticalMin = this.gameManager.UnlockableDatas[this.gameManager.ThemeID].VerticalMin;
            verticalMax = this.gameManager.UnlockableDatas[this.gameManager.ThemeID].VerticalMax;

            maxCount = this.gameManager.ThemeID + 1;
            if (this.gameManager.Continued)
            {
                spawnedObstacleCount = this.gameManager.ScoreValue;
            }
            else
            {
                spawnedObstacleCount = 0;
            }
            this.gameManager.BackgroundColor = this.gameManager.UnlockableDatas[this.gameManager.ThemeID].BackgroundColor;
            this.gameManager.ObstacleColor = this.gameManager.UnlockableDatas[this.gameManager.ThemeID].ObstacleColor;
        }

        superArray = new Obstacles[maxCount];
        var firstObstacleAdjustedCount = this.gameManager.UnlockableDatas[this.gameManager.ThemeID].PointsToFinishLevel - spawnedObstacleCount;
        for (int i = 0; i < maxCount; i++)
        {
            superArray[i] = new Obstacles(this.gameManager.UnlockableDatas[i]);
            GameObject prefab = superArray[i].unlockableData.NewObstacle;
            for (int j = 0; j < superArray[i].unlockableData.NewObstacleCount; j++)
            {
                GameObject go = Instantiate(prefab);
                go.SetActive(false);
                if (i == 0 && j < firstObstacleAdjustedCount)
                {
                    spawnedObstacleCount += 1;
                    verticalValue = Random.Range(verticalMin, verticalMax);
                    horizontalValue = Random.Range(1, 3) == 1 ? horizontalMin : horizontalMax;

                    if (horizontalValue == horizontalMin)
                    {
                        go.transform.localScale = new Vector2(-go.transform.localScale.x, go.transform.localScale.y);
                    }
                    go.transform.position = new Vector2(horizontalValue, lastVerticalValue + verticalValue);
                    go.SetActive(true);
                    lastVerticalValue = go.transform.position.y + go.GetComponent<Collider2D>().bounds.size.y;
                }
                superArray[i].obstacleQueue.Enqueue(go);
            }
        }
        
        // Spawning Level Finish game object if the point required to reach is less than 5.
        if (firstObstacleAdjustedCount < 5)
        {
            lastVerticalValue += 8f;
            levelFinish.transform.position = new Vector2(levelFinish.transform.position.x, lastVerticalValue);
            levelFinish.SetActive(true);
        }
    }

    public void SpawnObstacle()
    {
        if (!this.gameManager.InfiniteMode)
        {
            if (spawnedObstacleCount < this.gameManager.UnlockableDatas[this.gameManager.ThemeID].PointsToFinishLevel)
            {
                spawnedObstacleCount += 1;
                if (this.gameManager.ScoreValue < 5)
                {
                    GetObstacle(0);
                }
                else
                {
                    GetObstacle(Random.Range(0, maxCount));
                }
            }
            else if (!levelFinish.activeInHierarchy)
            {
                lastVerticalValue += 8f;
                levelFinish.transform.position = new Vector2(levelFinish.transform.position.x, lastVerticalValue);
                levelFinish.SetActive(true);
            }
            else if (levelFinish.activeInHierarchy)
            {
                return;
            }
        }
        else
        {
            var currentScore = this.gameManager.ScoreValue;

            if (currentScore < 5)
            {
                GetObstacle(0);
            }
            else if (currentScore >= 5 && currentScore < 10)
            {
                verticalMin = 5;
                verticalMax = 7;
                // Randomly chooses between verticalMin & verticalMax to place the new 
                // obstacle that much units above the previous obstacle.
                GetObstacle(shuffleIndex[Random.Range(0, (maxCount - (maxCount-2)))]);
            }
            else if (currentScore >= 10 && currentScore < 15)
            {
                verticalMin = 4;
                verticalMax = 6;
                GetObstacle(shuffleIndex[Random.Range(0, (maxCount - (maxCount - 3)))]);
            }
            else if (currentScore >= 15 && currentScore < 20)
            {
                GetObstacle(shuffleIndex[Random.Range(0, (maxCount - (maxCount - 4)))]);
            }
            else
            {
                GetObstacle(shuffleIndex[Random.Range(0, maxCount)]);
            }

            // General Formula to do above without if and else.
            /*if (this.gameManager.ScoreValue % 5 == 0)
            {
                if (verticalMin >= 4 && verticalMax >= 6)
                {
                    verticalMin -= 1;
                    verticalMax -= 1;
                }

                // Increase Random.Range's maximum value
            }*/
        }
    }

    void GetObstacle(int index)
    {
        if (superArray[index].obstacleQueue == null || superArray[index].obstacleQueue.Peek().activeInHierarchy)
        {
            index = 0;
        }
        if (superArray[index].unlockableData.NewObstacle.CompareTag("Obstacle Extra Space"))
        {
            lastVerticalValue += 2f;
        }

        GameObject dequeuedObj = superArray[index].obstacleQueue.Dequeue();

        verticalValue = Random.Range(verticalMin, verticalMax);
        horizontalValue = Random.Range(1, 3) == 1 ? horizontalMin : horizontalMax;
        if (horizontalValue == horizontalMin)
        {
            dequeuedObj.transform.localScale = new Vector2(-dequeuedObj.transform.localScale.x, dequeuedObj.transform.localScale.y);
        }
        dequeuedObj.transform.position = new Vector2(horizontalValue, lastVerticalValue + verticalValue);
        if (!dequeuedObj.activeSelf || !dequeuedObj.activeInHierarchy)
        {
            dequeuedObj.SetActive(true);
        }
      
        lastVerticalValue = dequeuedObj.transform.position.y + dequeuedObj.GetComponent<Collider2D>().bounds.size.y;
        superArray[index].obstacleQueue.Enqueue(dequeuedObj);
    }
}

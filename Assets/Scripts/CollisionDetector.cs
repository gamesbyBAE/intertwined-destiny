﻿using UnityEngine;

public class CollisionDetector : MonoBehaviour
{
    [SerializeField] GameEvent getObstacle;
    GameObject go;

    bool gameOver;
    private void Start()
    {
        gameOver = false;
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if ((collision.CompareTag("Obstacle") || collision.CompareTag("Obstacle Extra Space")) && !gameOver)
        {
            go = collision.gameObject.transform.parent != null ? collision.gameObject.transform.parent.gameObject : collision.gameObject;
            go.GetComponent<EdgeCollider2D>().enabled = true;
            go.gameObject.SetActive(false);
            if (go.transform.localScale.x < 0)
            {
                go.transform.localScale = new Vector2(-go.transform.localScale.x, go.transform.localScale.y);
            }
            getObstacle.Raise();
        }
    }

    public void GameOvered()
    {
        gameOver = true;
    }
}

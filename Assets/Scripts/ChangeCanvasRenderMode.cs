﻿using UnityEngine;

public class ChangeCanvasRenderMode : MonoBehaviour
{
    [SerializeField] GameManager gameManager;
    Canvas canvas;
    void Start()
    {
        canvas = GetComponent<Canvas>();
        if (this.gameManager.ScoreType >=0 && this.gameManager.ScoreType < 3)
        { 
            canvas.renderMode = RenderMode.ScreenSpaceCamera;
        }
        else
        {
            canvas.renderMode = RenderMode.ScreenSpaceOverlay;
        }
    }
}

﻿using UnityEngine;

public class LevelComplete : MonoBehaviour
{
    [SerializeField] GameEvent levelComplete;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Player"))
        {
            LeanTween.delayedCall(0.5f, () => levelComplete.Raise());
        }
    }
}

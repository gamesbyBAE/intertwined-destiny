﻿using UnityEngine;
using GooglePlayGames;
using GooglePlayGames.BasicApi;

public class GPGSAuthenticator : MonoBehaviour
{
    public static PlayGamesPlatform platform;

    void Start()
    {
        if (platform == null)
        {
            PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder().Build();
            PlayGamesPlatform.InitializeInstance(config);
            platform = PlayGamesPlatform.Activate();
        }

        PlayGamesPlatform.Instance.Authenticate(SignInInteractivity.CanPromptOnce, null);
    }
}

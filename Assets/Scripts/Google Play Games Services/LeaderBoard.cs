﻿using UnityEngine;
using GooglePlayGames;
using GooglePlayGames.BasicApi;

public class LeaderBoard : MonoBehaviour
{
    [SerializeField] GameManager gameManager;
    public void DisplayLeaderBoard()
    {
        PlayGamesPlatform.Instance.ShowLeaderboardUI(GPGSIds.leaderboard_high_score);
        if (PlayGamesPlatform.Instance.IsAuthenticated())
        {
            PlayGamesPlatform.Instance.ShowLeaderboardUI(GPGSIds.leaderboard_high_score);
        }
        else
        {
            PlayGamesPlatform.Instance.Authenticate(SignInInteractivity.CanPromptOnce, (success) =>
            {
                if (success == SignInStatus.Success)
                {
                    PlayGamesPlatform.Instance.ShowLeaderboardUI(GPGSIds.leaderboard_high_score);
                }
            });
        }
    }

    public void UpdateLeaderBoard()
    {
        Social.ReportScore(this.gameManager.ScoreValue, GPGSIds.leaderboard_high_score, null);
        if (PlayGamesPlatform.Instance.IsAuthenticated())
        {
            Social.ReportScore(this.gameManager.ScoreValue, GPGSIds.leaderboard_high_score, null);
        }
    }
}
﻿using UnityEngine;

public class ConfettiPop : MonoBehaviour
{
    ParticleSystem confetti;

    private void OnEnable()
    {
        confetti = GetComponent<ParticleSystem>();
    }

    public void PopConfetti()
    {
        confetti.Play();
    }
}

﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverMenuGUI : MonoBehaviour
{
    [SerializeField] GameManager gameManager;
    [SerializeField] GameEvent panelVisible;
    [SerializeField] GameEvent deductSoftCurrency;
    [SerializeField] GameObject canvas;

    [Header("Changing Alpha of BG for Transition")]
    [SerializeField] GameObject transitionBG;
    RectTransform transitionBGRectTransform;
    [SerializeField] float alphaTweenTime;

    [Header("Rescaling Game Over GUI")]
    [SerializeField] RectTransform panelRectTransform;
    [SerializeField] Vector3 targetScale;
    [SerializeField] Vector3 origScale;
    [SerializeField] float scalingTweenTime;

    void Awake()
    {
        this.transitionBGRectTransform = transitionBG.GetComponent<RectTransform>();
        LeanTween.alpha(this.transitionBGRectTransform, 0, alphaTweenTime).setIgnoreTimeScale(true).setDelay(0.2f).setOnComplete(() => { canvas.SetActive(false); });
    }

    public void EnableCanvas(bool levelFailed)
    {
        float delayTime = 0.5f;
        if (!levelFailed)
        {
            this.gameManager.ThemeID += 1;
            if (this.gameManager.ThemeID == this.gameManager.UnlockableDatas.Length)
            {
                this.gameManager.ThemeID = 0;
            }
            delayTime = 1f;
        }
        EncryptDecryptData.EncryptData(this.gameManager);
        canvas.SetActive(true);
        //notifier.SetActive(true);
        transitionBG.SetActive(false);
        LeanTween.scale(panelRectTransform, targetScale, scalingTweenTime).setDelay(delayTime).setEaseOutBack().setIgnoreTimeScale(true)
            .setOnComplete(() =>
            { 
                if (panelVisible) 
                { 
                    panelVisible.Raise();
                }

                /*if (SceneManager.GetActiveScene().name.Equals("2 Challenge Mode Scene"))
                {
                    ToggleNAGames.Instance.SetCanvasActive(true);
                }*/
            });

        
        //Time.timeScale = 0;
    }

    // Canvas Disabler.
    public void DisableCanvas(bool restarted)
    {
        // Without it SceneLoader won't be called. Need to set it back to 1 if in future Time Scale is ever set to 0.
        //Time.timeScale = 1; 

        if (!this.gameManager.InfiniteMode && restarted && this.gameManager.SoftCurrency < this.gameManager.ChallengeCost)
        {
            //Notify not enough credits to restart.
            return;
        }
        else if (!this.gameManager.InfiniteMode && restarted && this.gameManager.SoftCurrency >= this.gameManager.ChallengeCost)
        {
            // Deduct Soft Currency.
            deductSoftCurrency.Raise();
        }

        var delayTime = 0.2f;
        /*if (restarted)
        {
            delayTime = 1f;
        }*/
        var seq = LeanTween.sequence();
        seq.append(delayTime);
        seq.append(LeanTween.scale(panelRectTransform, origScale, scalingTweenTime).setEaseInBack().setIgnoreTimeScale(true).setOnCompleteParam(Time.timeScale = 1));
        seq.append(() =>
        {
            if (restarted)
            {
               this.gameObject.GetComponent<SceneLoader>().Restart();
            }
            else
            {
                this.gameObject.GetComponent<SceneLoader>().Continue();
            }
        });

    }

    // Canvas Disabler when a Level of Challenge Mode is completed.
    public void DisableCanvas()
    {
        this.gameManager.NextLevel = true;
        var seq = LeanTween.sequence();
        seq.append(0.2f);
        seq.append(LeanTween.scale(panelRectTransform, origScale, scalingTweenTime).setEaseInBack().setIgnoreTimeScale(true).setOnCompleteParam(Time.timeScale = 1));
        seq.append(() => 
        {
            this.gameObject.GetComponent<SceneLoader>().ChallengeRun();
        });
    }
}

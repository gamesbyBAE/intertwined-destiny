﻿using UnityEngine;
using UnityEngine.UI;

public class ToggleAudioOnOff : MonoBehaviour
{
    enum AudioType
    {
        music,
        sfx
    }
    [SerializeField]
    AudioType audioType;

    [SerializeField]
    Toggle toggleButton;

    [SerializeField]
    GameEvent muteAudio, unMuteAudio;

    [SerializeField]
    GameManager gameManager;

    private void Start()
    {
        toggleButton = GetComponent<Toggle>();
        if (audioType == AudioType.music)
        {
            toggleButton.isOn = gameManager.MuteMusic;
        }
        else if (audioType == AudioType.sfx)
        {
            toggleButton.isOn = gameManager.MuteSFX;
        }
    }

    public void CheckIsOn(bool boolValue)
    {
        if (boolValue)
        {
            Mute();
        }
        else if (!boolValue)
        {
            Unmute();
        }
    }
    void Mute()
    {
        if (audioType == AudioType.music)
        {
            gameManager.MuteMusic = true;
        }
        else if (audioType == AudioType.sfx)
        {
            gameManager.MuteSFX = true;
        }
        muteAudio.Raise(); 
    }

    void Unmute()
    {
        if (audioType == AudioType.music)
        {
            gameManager.MuteMusic = false;
        }
        else if (audioType == AudioType.sfx)
        {
            gameManager.MuteSFX = false;
        }
        unMuteAudio.Raise();
    }
}

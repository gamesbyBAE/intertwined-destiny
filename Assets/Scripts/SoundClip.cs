﻿using UnityEngine;
[CreateAssetMenu (fileName = "New Sound Clip", menuName = "Sound/Sound Clip", order = 60)]
public class SoundClip : ScriptableObject
{
    public enum SoundListEnum
    {
        ScoreChime,
        BackgroundTheme,
        HighScoreTune,
        GameOver,
        SkinUnlocked,
        Count
    }
    public SoundListEnum soundListType;
    
    [SerializeField]
    AudioClip clip;

    [SerializeField] [Range(0f, 1f)]
    float volume;

    [SerializeField]
    bool loop;
    
    [SerializeField]
    bool startAtAwake;

    #region Setters & Getters
    public AudioClip Clip { get { return clip; } }
    
    public float Volume { get { return volume; } }

    public bool Loop { get { return loop; } }

    public bool StartAtAwake { get { return startAtAwake; } }
    #endregion
}

﻿using UnityEngine;
public class StartingText : MonoBehaviour
{   
    [SerializeField] GameManager gameManager;
    //[SerializeField] GameObject go;

    int count;
    private void Start()
    {
        count = 5;
    }
    public void DestroyAfterDelay()
    {
        count -= 1;
        if(count == 0)
        {
            Destroy(this.gameObject);
        }
    }
}

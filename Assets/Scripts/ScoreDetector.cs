﻿using UnityEngine;

public class ScoreDetector : MonoBehaviour
{
    [SerializeField]
    GameEvent onPointScored;

    [SerializeField]
    GameManager gameManager;

    EdgeCollider2D edgeCol;
    bool touchedEdgeCol;
    private void Start()
    {
        touchedEdgeCol = false;
        edgeCol = GetComponent<EdgeCollider2D>();
    }

    // Checking if the player has crossed the EdgeCollider trigger that's supposed to be crossed to get points
    // and not crossing any other Collider set as a Trigger.
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player") && collision.IsTouching(edgeCol))
        {
            touchedEdgeCol = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player") && edgeCol.enabled && touchedEdgeCol)
        {
            edgeCol.enabled = false;
            touchedEdgeCol = false;
            gameManager.ScoreValue += 1;
            onPointScored.Raise();
        }
    }
}

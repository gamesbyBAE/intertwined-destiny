﻿using UnityEngine;
using UnityEngine.UI;
public class OpenCloseMenu : MonoBehaviour
{
    [SerializeField]
    float xOriginalPosition, xTargetPosition, slidingTime;

    [SerializeField]
    GameEvent openMenu;

    RectTransform rectTransform;

    [SerializeField]
    Toggle menuButton;

    void Start()
    {
        rectTransform = GetComponent<RectTransform>();
        xOriginalPosition = rectTransform.anchoredPosition.x;
    }
    public void TweenMenu(bool toggleState)
    {
        float pos = 0f;
        if (toggleState)
        {
            pos = xTargetPosition;
            openMenu.Raise();
        }
        else if (!toggleState)
        {
            menuButton.isOn = false;
            pos = xOriginalPosition;
        }

        LeanTween.moveX(rectTransform, pos, slidingTime).setEaseOutCubic();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class String : MonoBehaviour
{
    public Rigidbody2D StartPoint;
    public Rigidbody2D EndPoint;

    private LineRenderer lineRenderer;
    private List<RopeSegment> ropeSegments = new List<RopeSegment>();
    public float ropeSegLen = 0.10f;
    public int segmentLength = 35;
    public float lineWidth = 0.1f;

    PlayerController getValFromMovement;
    int ballSwitcher;
    // Use this for initialization
    void Awake()
    {
        this.lineRenderer = this.GetComponent<LineRenderer>();
        Vector2 ropeStartPoint = StartPoint.position;

        getValFromMovement = GameObject.Find("Player Controller").GetComponent<PlayerController>();
        ballSwitcher = getValFromMovement.SwitchValue;

        for (int i = 0; i < segmentLength; i++)
        {
            this.ropeSegments.Add(new RopeSegment(ropeStartPoint));
            ropeStartPoint.x -= ropeSegLen;
        }
        //ApplyConstraint();
        DrawRope();
    }

    // Update is called once per frame
    void Update()
    {
        ballSwitcher = getValFromMovement.SwitchValue;

        /*EndPoint.position = ropeSegments[ropeSegments.Count - 1].posNow;
        StartPoint.position = ropeSegments[0].posNow;*/
        
        this.DrawRope();
        this.Simulate();

        if (ballSwitcher == 1)
        {
            //EndPoint.MovePosition(ropeSegments[ropeSegments.Count - 1].posNow);
            EndPoint.gameObject.transform.position = ropeSegments[ropeSegments.Count - 1].posNow;
            //LeanTween.move(EndPoint.gameObject, ropeSegments[ropeSegments.Count - 1].posNow, 0.095f);
        }
        else if (ballSwitcher == -1)
        {
            //StartPoint.MovePosition(ropeSegments[0].posNow);
            StartPoint.gameObject.transform.position = ropeSegments[0].posNow;
            //LeanTween.move(StartPoint.gameObject, ropeSegments[0].posNow, 0.095f);
        }
        else if (ballSwitcher == 0)
        {
            EndPoint.MovePosition(ropeSegments[ropeSegments.Count - 1].posNow);
            StartPoint.MovePosition(ropeSegments[0].posNow);
            //EndPoint.position = Vector2.Lerp(EndPoint.position, ropeSegments[ropeSegments.Count - 1].posNow, 5 * Time.deltaTime);
            //StartPoint.position = Vector2.Lerp(StartPoint.position, ropeSegments[0].posNow, 5 * Time.deltaTime);
        }
    }

/*    private void FixedUpdate()
    {
        this.DrawRope();
        this.Simulate();

        if (ballSwitcher == 1)
        {
            EndPoint.MovePosition(ropeSegments[ropeSegments.Count - 1].posNow);
            //EndPoint.position = Vector2.Lerp(EndPoint.position, ropeSegments[ropeSegments.Count - 1].posNow, 1);
        }
        else if (ballSwitcher == -1)
        {
            StartPoint.MovePosition(ropeSegments[0].posNow);
            //StartPoint.position = Vector2.Lerp(StartPoint.position, ropeSegments[0].posNow, 5*Time.deltaTime); 
        }
        else if (ballSwitcher == 0)
        {
            EndPoint.MovePosition(ropeSegments[ropeSegments.Count - 1].posNow);
            StartPoint.MovePosition(ropeSegments[0].posNow);
            //EndPoint.position = Vector2.Lerp(EndPoint.position, ropeSegments[ropeSegments.Count - 1].posNow, 5 * Time.deltaTime);
            //StartPoint.position = Vector2.Lerp(StartPoint.position, ropeSegments[0].posNow, 5 * Time.deltaTime);
        }
    }*/

    private void Simulate()
    {
        RopeSegment begSegment = this.ropeSegments[0];
        begSegment.posNow = this.StartPoint.position;
        this.ropeSegments[0] = begSegment;

        RopeSegment endSegment = this.ropeSegments[this.ropeSegments.Count - 1];
        endSegment.posNow = this.EndPoint.position;
        this.ropeSegments[this.ropeSegments.Count - 1] = endSegment;

        if (ballSwitcher == 1 || ballSwitcher == 0)
        {
            Vector2 forceGravity = new Vector2(0, -0.1f);

            for (int i = 1; i < this.segmentLength - 1; i++)
            {
                RopeSegment firstSegment = this.ropeSegments[i];
                Vector2 velocity = firstSegment.posNow - firstSegment.posOld;
                firstSegment.posOld = firstSegment.posNow;
                firstSegment.posNow += velocity;
                firstSegment.posNow += forceGravity * Time.deltaTime;
                this.ropeSegments[i] = firstSegment;
            }
        }
        else if (ballSwitcher == -1)
        {
            Vector2 forceGravity = new Vector2(0, -0.1f);
            for (int i = this.segmentLength - 2; i > 0; i--)
            {
                RopeSegment firstSegment = this.ropeSegments[i];
                Vector2 velocity = firstSegment.posNow - firstSegment.posOld;
                firstSegment.posOld = firstSegment.posNow;
                firstSegment.posNow += velocity;
                firstSegment.posNow += forceGravity * Time.deltaTime;
                this.ropeSegments[i] = firstSegment;
            }
        }

        //CONSTRAINTS
        for (int i = 0; i < 80; i++)
        {
            this.ApplyConstraint();
        }
    }

    private void ApplyConstraint()
    {
        if(ballSwitcher == -1)
        {
            RopeSegment endSegment = this.ropeSegments[this.ropeSegments.Count - 1];
            endSegment.posNow = this.EndPoint.position;
            this.ropeSegments[this.ropeSegments.Count - 1] = endSegment;

            for (int i = this.segmentLength - 1; i > 0; i--)
            {
                RopeSegment firstSeg = this.ropeSegments[i];
                RopeSegment secondSeg = this.ropeSegments[i - 1];

                float dist = (firstSeg.posNow - secondSeg.posNow).magnitude;
                float error = Mathf.Abs(dist - this.ropeSegLen);
                //Vector2 changeDir = (firstSeg.posNow - secondSeg.posNow).normalized;
                Vector2 changeDir = Vector2.zero;

                if (dist > ropeSegLen)
                {
                    changeDir = (firstSeg.posNow - secondSeg.posNow).normalized;
                }
                else if (dist < ropeSegLen)
                {
                    changeDir = (secondSeg.posNow - firstSeg.posNow).normalized;
                }

                Vector2 changeAmount = changeDir * error;

                /*if (i != segmentLength - 1)
                {*/
                firstSeg.posNow -= changeAmount * 0.5f;
                this.ropeSegments[i] = firstSeg;
                secondSeg.posNow += changeAmount * 0.5f;
                this.ropeSegments[i - 1] = secondSeg;
                /*}
                else
                {
                    secondSeg.posNow += changeAmount;
                    this.ropeSegments[i - 1] = secondSeg;
                }*/
            }
        }
        else
        {
            if (ballSwitcher == 0)
            {
                RopeSegment firstSegment = this.ropeSegments[0];
                firstSegment.posNow = this.StartPoint.position;
                this.ropeSegments[0] = firstSegment;

                RopeSegment endSegment = this.ropeSegments[this.ropeSegments.Count - 1];
                endSegment.posNow = this.EndPoint.position;
                this.ropeSegments[this.ropeSegments.Count - 1] = endSegment;
            }
            else if (ballSwitcher == 1)
            {
                RopeSegment firstSegment = this.ropeSegments[0];
                firstSegment.posNow = this.StartPoint.position;
                this.ropeSegments[0] = firstSegment;
            }

            for (int i = 0; i < this.segmentLength - 1; i++)
            {
                RopeSegment firstSeg = this.ropeSegments[i];
                RopeSegment secondSeg = this.ropeSegments[i + 1];

                float dist = (firstSeg.posNow - secondSeg.posNow).magnitude;
                float error = Mathf.Abs(dist - this.ropeSegLen);
                //Vector2 changeDir = (firstSeg.posNow - secondSeg.posNow).normalized;
                Vector2 changeDir = Vector2.zero;

                if (dist > ropeSegLen)
                {
                    changeDir = (firstSeg.posNow - secondSeg.posNow).normalized;
                }
                else if (dist < ropeSegLen)
                {
                    changeDir = (secondSeg.posNow - firstSeg.posNow).normalized;

                }

                Vector2 changeAmount = changeDir * error;

                /*if (i != 0)
                {*/
                firstSeg.posNow -= changeAmount * 0.5f;
                this.ropeSegments[i] = firstSeg;
                secondSeg.posNow += changeAmount * 0.5f;
                this.ropeSegments[i + 1] = secondSeg;
                /*}
                    else
                {
                    secondSeg.posNow += changeAmount;
                    this.ropeSegments[i + 1] = secondSeg;
                }*/
            }
        }


    }

    private void DrawRope()
    {
        float lineWidth = this.lineWidth;
        lineRenderer.startWidth = lineWidth;
        lineRenderer.endWidth = lineWidth;

        Vector3[] ropePositions = new Vector3[this.segmentLength];

        for (int i = 0; i < this.segmentLength; i++)
        {
            ropePositions[i] = this.ropeSegments[i].posNow;
        }
        lineRenderer.positionCount = ropePositions.Length;
        lineRenderer.SetPositions(ropePositions);
    }

    public struct RopeSegment
    {
        public Vector2 posNow;
        public Vector2 posOld;

        public RopeSegment(Vector2 pos)
        {
            this.posNow = pos;
            this.posOld = pos;
        }
    }
}

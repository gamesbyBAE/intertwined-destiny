﻿using UnityEngine;

public class SlideHolders : MonoBehaviour
{
    [SerializeField]
    float rightOffset, rightPos, slideDist, slideDuration;

    float leftOffset, leftPos;

    RectTransform rt;

    void Start()
    {
        rt = GetComponent<RectTransform>();
        leftOffset = -rightOffset;
        leftPos = -rightPos;
    }

    public void SlideRight()
    {
        if (this.rt.anchoredPosition.x > rightOffset)
        {
            this.rt.anchoredPosition = new Vector2(leftPos, this.rt.anchoredPosition.y);
        }

        SlideCard(slideDist);
    }

    public void SlideLeft()
    {
        if (this.rt.anchoredPosition.x < leftOffset)
        {
            this.rt.anchoredPosition = new Vector2(rightPos, this.rt.anchoredPosition.y);
        }

        SlideCard(-slideDist);
    }

    void SlideCard(float dist)
    {
        LeanTween.moveX(rt, rt.anchoredPosition.x + dist, slideDuration).setEaseOutCubic();
    }
}

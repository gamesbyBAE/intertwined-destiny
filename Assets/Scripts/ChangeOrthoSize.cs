﻿using UnityEngine;

public class ChangeOrthoSize : MonoBehaviour
{
    [SerializeField] float desiredWidth;
    Camera mainCam;

    void Awake()
    {
        mainCam = GetComponent<Camera>();
        mainCam.orthographicSize = desiredWidth / mainCam.aspect;
    }
}

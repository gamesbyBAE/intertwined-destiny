﻿using UnityEngine;
using System.Collections;

public class DoubleTapExit : MonoBehaviour
{
    [SerializeField] GameManager gameManager;
    [SerializeField] GameEvent quitPressed;
    [SerializeField] GameObject canvasGO;
    [SerializeField] GameObject exitImage;
    [SerializeField] float waitTime;
    [SerializeField] int tweenTargetPosition;
    [SerializeField] float tweenTime;

    RectTransform exitRT;
    int currentPosition;
    bool pressedOnce;
    IEnumerator coroutine;
    void Start()
    {
        pressedOnce = false;
        exitRT = exitImage.GetComponent<RectTransform>();
        currentPosition = (int)exitRT.anchoredPosition.y;
        canvasGO.SetActive(false);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && !pressedOnce)
        {   
            RollUp();
            quitPressed.Raise();
        }
    }

    IEnumerator WaitCountdown()
    {
        yield return null;
        float timer = 0f;
        while (timer < waitTime)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                EncryptDecryptData.EncryptData(this.gameManager);
                Application.Quit();
            }
            else if (Input.touchCount > 0 || Input.GetMouseButton(1))
            {
                RollDown();
            }
            timer += Time.fixedDeltaTime;
            yield return null;
        }
        RollDown();
    }
     void RollUp()
    {
        canvasGO.SetActive(true);
        pressedOnce = true;
        coroutine = WaitCountdown();
        StartCoroutine(this.coroutine);
        LeanTween.moveY(exitRT, tweenTargetPosition, tweenTime).setEaseInCubic().setIgnoreTimeScale(true);
    }
    void RollDown()
    {
        pressedOnce = false;
        StopCoroutine(coroutine);
        var seq = LeanTween.sequence();
        seq.append(LeanTween.moveY(exitRT, currentPosition, tweenTime).setEaseInCubic().setIgnoreTimeScale(true));
        seq.append(LeanTween.delayedCall(0f, () => canvasGO.SetActive(false)).setIgnoreTimeScale(true));
    }
}

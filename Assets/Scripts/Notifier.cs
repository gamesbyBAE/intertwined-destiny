﻿using UnityEngine;

public class Notifier : MonoBehaviour
{
    [SerializeField] GameManager gameManager;

    [SerializeField] float rotationAngle, jiggleDuration, jiggleRepeatDelayDuration;

    bool unlockedAll;
    private void OnEnable()
    {
        unlockedAll = true;
        Check();
    }

    public void Check()
    {
        for (int i = 0; i < this.gameManager.SkinDatas.Length; i++)
        {
            if (!this.gameManager.SkinDatas[i].Unlocked)
            {
                unlockedAll = false;
                break;
            }
        }
        if (!unlockedAll && (this.gameManager.SoftCurrency >= 200))
        {
            this.gameObject.SetActive(true);
            JiggleNotifier();
            Notification();
        }
        else
        {
            this.gameObject.SetActive(false);
        }
    }

    void JiggleNotifier()
    {
        int distributeJiggleDuration = 6;

        var seq = LeanTween.sequence();

        seq.append(LeanTween.rotateZ(this.gameObject, -rotationAngle, jiggleDuration / distributeJiggleDuration));
        seq.append(LeanTween.rotateZ(this.gameObject, rotationAngle, jiggleDuration / distributeJiggleDuration ));
        seq.append(LeanTween.rotateZ(this.gameObject, 0, jiggleDuration / distributeJiggleDuration ));


        seq.append(LeanTween.rotateZ(this.gameObject, -rotationAngle, jiggleDuration / distributeJiggleDuration));
        seq.append(LeanTween.rotateZ(this.gameObject, rotationAngle, jiggleDuration / distributeJiggleDuration));
        seq.append(LeanTween.rotateZ(this.gameObject, 0, jiggleDuration / distributeJiggleDuration));
    }

    void Notification()
    {
        LeanTween.delayedCall(jiggleRepeatDelayDuration, () => JiggleNotifier()).setRepeat(-1).setOnCompleteOnRepeat(true);
    }

    private void OnDisable()
    {
        LeanTween.cancel(this.gameObject);
    }
}

﻿using UnityEngine;

public class RotatingMotion : MonoBehaviour
{
    [SerializeField] float oneRotationDuration;
    void OnEnable ()
    {
        if (this.gameObject.transform.position.x > 0)
        {
            RotateObject(Vector3.forward);
        }
        else if (this.gameObject.transform.position.x < 0)
        {
            RotateObject(Vector3.back);
        }
    }

    void OnDisable()
    {
        LeanTween.cancel(this.gameObject);
    }

    void RotateObject(Vector3 direction)
    {
        LeanTween.rotateAround(this.gameObject, -direction, 360, oneRotationDuration).setRepeat(-1);
    }
}

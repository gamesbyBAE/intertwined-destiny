﻿using UnityEngine;

public class SwayMotion : MonoBehaviour
{
    //Need to change everything to private in the end
    [SerializeField] float swayDist, swayDuration;
    [SerializeField] LeanTweenType easeType;
    [SerializeField] bool resetOnDisable;
    float originalXPosition = 0;

    private void OnEnable ()
    {
        if (resetOnDisable && originalXPosition == 0)
        {
            this.originalXPosition = this.gameObject.transform.localPosition.x;
        }

        if (this.gameObject.transform.position.x > 0)
        {
            Sway(swayDist);
        }
        else if (this.gameObject.transform.position.x < 0)
        {
            Sway(-swayDist);
        }
    }

    private void OnDisable()
    {
        LeanTween.cancel(this.gameObject);
        if (resetOnDisable)
        {
            this.gameObject.transform.localPosition = new Vector3(this.originalXPosition, this.gameObject.transform.localPosition.y, this.gameObject.transform.localPosition.z);
        }
    }

    void Sway(float signedSwayDist)
    {
        LeanTween.moveX(this.gameObject, this.gameObject.transform.position.x + signedSwayDist, swayDuration).setEase(easeType).setLoopPingPong(-1);
    }
}

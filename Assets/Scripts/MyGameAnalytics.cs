﻿using UnityEngine;
using GameAnalyticsSDK;

public class MyGameAnalytics : MonoBehaviour
{
    [SerializeField] GameManager gameManager;

    [SerializeField] bool testMode;
    //GameEventListener gameOverListener;
    
    void Start()
    {
        if (!testMode)
        {
            GameAnalytics.Initialize(); //Remove comment when done with testing in development phase.
        }

        /*
         * To implement GameEventListeners via script rather than manually in inspector (continued from GameEventListener.cs)
        */
        //gameOverListener.GetComponent<GameEventListener>();
        //gameOverListener.response.AddListener(() => PointScored());
    }

    // How many times user restarted/played the Infinite Mode.
    public void PlayCount()
    {
        GameAnalytics.NewDesignEvent("Play Count", this.gameManager.PlayCount);
    }

    // The level completed by the user in Challenge Mode.
    public void LevelCompletedID()
    {
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, "Completed Level", this.gameManager.ThemeID-1);
    }
}

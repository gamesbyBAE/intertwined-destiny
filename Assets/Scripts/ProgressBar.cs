﻿using UnityEngine;
using UnityEngine.UI;

public class ProgressBar : MonoBehaviour
{
    [SerializeField] GameManager gameManager;
    [SerializeField] GameObject mask;
    [SerializeField] float fillTime;

    Image maskImage;
    int maxScore;
    float fillValue;

    void Start()
    {
        maxScore = this.gameManager.UnlockableDatas[this.gameManager.ThemeID].PointsToFinishLevel;
        maskImage = mask.GetComponent<Image>();
        maskImage.fillAmount = (float)this.gameManager.ScoreValue / (float)maxScore;
    }

    public void UpdateProgressBar()
    {
        if (LeanTween.isTweening(mask))
        {
            LeanTween.cancel(mask);
        }
        fillValue = (float)this.gameManager.ScoreValue / (float)maxScore;
        LeanTween.value(mask,maskImage.fillAmount, fillValue, fillTime).setEaseOutQuint().setOnUpdate((float val) => maskImage.fillAmount = val);
    }
}

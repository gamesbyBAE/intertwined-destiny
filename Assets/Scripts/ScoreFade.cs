﻿using System.Collections;
using UnityEngine;
using TMPro;

public class ScoreFade : MonoBehaviour
{
    [SerializeField] Camera mainCam;
    [SerializeField] GameManager gameManager;
    [SerializeField] float threshHoldDistance;

    TextMeshProUGUI textField;
    IEnumerator coroutine;

    void Start()
    {
        if (this.gameObject.activeSelf)
        {
            textField = GetComponent<TextMeshProUGUI>();
            textField.color = new Color(textField.color.r, textField.color.g, textField.color.b, 0);
            this.coroutine = FadeInText(threshHoldDistance);
            StartCoroutine(this.coroutine);
        }
    }

    IEnumerator FadeInText(float dist)
    {
        while (mainCam.transform.position.y < dist)
        {
            yield return null;
        }
        LeanTween.value(0, 1, 0.5f).setOnUpdate((float originalAlpha) => textField.color = new Color(textField.color.r, textField.color.g, textField.color.b, originalAlpha));
        StopCoroutine(this.coroutine);
    }
}

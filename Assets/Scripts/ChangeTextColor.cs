﻿using UnityEngine;
using TMPro;

public class ChangeTextColor : MonoBehaviour
{
    [SerializeField] GameManager gameManager;
    [SerializeField] [Range(0f,1f)] float darkValue;
    Color textColor;
    float h, s, v;
    TextMeshProUGUI textToChangeColor;
    
    void Start()
    {
        Color.RGBToHSV(this.gameManager.BackgroundColor, out h, out s, out v);
        textColor = Color.HSVToRGB(h, s, v - darkValue);

        this.textToChangeColor = GetComponent<TextMeshProUGUI>();
        this.textToChangeColor.color = textColor;
    }
}

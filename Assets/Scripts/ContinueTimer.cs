﻿using UnityEngine;
using TMPro;
using System.Collections;
using UnityEngine.UI;

public class ContinueTimer : MonoBehaviour
{
    [SerializeField] GameManager gameManager;
    [SerializeField] GameEvent timeOver;
    [SerializeField] Image timerCircle;
    [SerializeField] float timer;

    float maxFillAmount;
    int wholeTimer;
    TextMeshProUGUI timerText;
    IEnumerator coroutine;
    void Start()
    {
        if (this.gameManager.Continued)
        {
            timeOver.Raise();
        }
        maxFillAmount = timer;
        wholeTimer = (int)timer;
        timerText = GetComponent<TextMeshProUGUI>();
        timerText.text = timer.ToString();
        coroutine = Countdown();
    }

    public void StartCountdown()
    {
        StartCoroutine(coroutine);
    }
    IEnumerator Countdown()
    {
        while (timer > 0)
        {
            timer -= Time.deltaTime;
            timerCircle.fillAmount = timer / maxFillAmount;
            if ((int)timer < wholeTimer)
            {
                wholeTimer -= 1;
                timerText.text = timer.ToString("0");
            }
            yield return null;
        }
        timeOver.Raise();
        StopCountdown();
    }

    public void StopCountdown()
    {
        StopCoroutine(coroutine);
    }
}

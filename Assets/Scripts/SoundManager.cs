﻿using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public static SoundManager instance;

    [SerializeField] GameManager gameManager;
    [SerializeField] SoundClip[] sounds;

    AudioSource[] audioSources;

    void Awake()
    {
        #region Creating Singleton
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
            return;
        }
        #endregion

        audioSources = new AudioSource[(int)SoundClip.SoundListEnum.Count];

        for (int i = 0; i < sounds.Length; i++)
        {
            int j = (int)sounds[i].soundListType;

            audioSources[j] = gameObject.AddComponent<AudioSource>();
            audioSources[j].enabled = false;
            audioSources[j].clip = sounds[i].Clip;
            audioSources[j].volume = sounds[i].Volume;
            audioSources[j].loop = sounds[i].Loop;
            if((sounds[i].soundListType == SoundClip.SoundListEnum.BackgroundTheme) && this.gameManager.MuteMusic)
            {
                audioSources[j].mute = true;
            }
            else if(this.gameManager.MuteSFX)
            {
                audioSources[j].mute = true;
            }
            audioSources[j].playOnAwake = sounds[i].StartAtAwake;
            audioSources[j].enabled = true;
        }
    }

    public void Play(SoundClip sc)
    {
        int i = GetIndex(sc);
        audioSources[i].Play();
    }

    public void MuteAudioSources(SoundClipCategory scc)
    {
        MuteUnmuteAudioSource(scc, true);
    }

    public void UnMuteAudioSources(SoundClipCategory scc)
    {
        MuteUnmuteAudioSource(scc, false);

    }

    void MuteUnmuteAudioSource(SoundClipCategory scc, bool boolValue)
    {
        SoundClip[] soundClip = scc.SoundClips;
        for (int i = 0; i < soundClip.Length; i++)
        {
            int j = GetIndex(soundClip[i]);
            audioSources[j].mute = boolValue;
        }
    }

    public AudioSource GetAudioSource(SoundClip.SoundListEnum sle)
    {
        int i = (int)sle;
        return audioSources[i];
    }

    int GetIndex(SoundClip sc)
    {
        return (int)sc.soundListType;
    }
}

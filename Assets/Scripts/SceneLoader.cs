﻿using UnityEngine.SceneManagement;
using UnityEngine;

public class SceneLoader : MonoBehaviour
{
    [SerializeField] GameManager gameManager;
    [SerializeField] GameObject transitionBG;
    RectTransform transitionBGRectTransform;
    [SerializeField] float fadeTime;

    bool touchedScreen;

    string homeScene = "0 Home Scene";
    string infiniteMode = "1 Infinite Mode Scene";
    string challengeMode = "2 Challenge Mode Scene";
    string settingsScene = "3 Settings Scene";
    string aboutScene = "4 About Scene";
    string customizeScene = "5 Customize Scene";

    private void Awake()
    {
        if (SceneManager.GetActiveScene().name == infiniteMode || SceneManager.GetActiveScene().name == challengeMode)
        {
            touchedScreen = false;
            Time.timeScale = 0;
        }
    }

    private void Start()
    {
        transitionBGRectTransform = transitionBG.GetComponent<RectTransform>();
        if (SceneManager.GetActiveScene().name != infiniteMode || SceneManager.GetActiveScene().name != challengeMode)
        {
            LeanTween.alpha(transitionBGRectTransform, 0, fadeTime).setIgnoreTimeScale(true).setOnComplete(() => { transitionBG.SetActive(false); });
        }
    }
    private void Update()
    {
        if (!touchedScreen && (SceneManager.GetActiveScene().name == infiniteMode || SceneManager.GetActiveScene().name == challengeMode) && (Input.touchCount > 0 || Input.GetMouseButton(1)))
        {
            touchedScreen = true;
            Time.timeScale = 1;
        }
    }

    public void InfiniteRun()
    {
        this.gameManager.FirstTime = false;
        this.gameManager.InfiniteMode = true;
        //this.gameManager.inputType = 1;
        this.gameManager.PlayCount += 1;
        this.gameManager.ScoreValue = 0;
        this.gameManager.WatchedAd = false;
        this.gameManager.Continued = false;
        FadeOut(() => { SceneManager.LoadScene(infiniteMode); });
    }

    public void ChallengeRun()
    {
        if (this.gameManager.SoftCurrency < 10)
        {
            //Notify not enough credits to restart.
            return;
        }
        else
        {
            if (!this.gameManager.NextLevel)
            {
                this.gameManager.SoftCurrency -= 10;
            }
            this.gameManager.FirstTime = false;
            this.gameManager.InfiniteMode = false;
            //this.gameManager.inputType = 1;
            this.gameManager.ScoreValue = 0;
            this.gameManager.WatchedAd = false;
            this.gameManager.Continued = false;
            this.gameManager.NextLevel = false;
            FadeOut(() => { SceneManager.LoadScene(challengeMode); });
        }
        
    }
    public void Restart()
    {
        this.gameManager.ScoreValue = 0;
        this.gameManager.WatchedAd = false;
        this.gameManager.Continued = false;
        this.gameManager.PlayCount += 1;
        FadeOut(() => { SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex); });
    }
    public void Continue()
    {
        this.gameManager.WatchedAd = true;
        this.gameManager.Continued = true;
        FadeOut(() => { SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex); });
    }

    public void Home()
    {
        FadeOut(() => { SceneManager.LoadScene(homeScene); });
    }
    public void Store()
    {
        FadeOut(() => { SceneManager.LoadScene(customizeScene); });
    }

    public void Settings()
    {
        FadeOut(() => { SceneManager.LoadScene(settingsScene); });
    }

    public void About()
    {
        FadeOut(() => { SceneManager.LoadScene(aboutScene); });
    }

    void FadeOut(System.Action callback)
    {
        LeanTween.cancelAll();
        if (Time.timeScale == 0)
        {
            Time.timeScale = 1;
        }
        EncryptDecryptData.EncryptData(gameManager);
        var seq = LeanTween.sequence();
        transitionBG.SetActive(true);
        seq.append(LeanTween.alpha(transitionBGRectTransform, 1, fadeTime));
        seq.append(() => { callback(); });
    }
}

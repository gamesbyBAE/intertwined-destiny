﻿using UnityEngine;
using UnityEngine.UI;

public class SkinHandler : MonoBehaviour
{
    [SerializeField] int skinNumber;
    [SerializeField] GameManager gameManager;
    [SerializeField] GameEvent skinValueChanged;
    public Toggle toggleButton;
    [SerializeField] GameObject questionMarkGO;
    [SerializeField] GameObject skinGO;
    [SerializeField] Image skinImage;

    bool rightBall;
    void Start()
    {
        rightBall = false;
        skinImage.sprite = this.gameManager.SkinDatas[skinNumber].SkinSprite;
        if (this.gameManager.SkinDatas[skinNumber].Unlocked)
        {
            ShowSkin();
        }
        else
        {
            HideSkin();
        }
    }

    public void ShowSkin()
    {
        toggleButton.interactable = true;
        toggleButton.onValueChanged.AddListener((bool val) => { SetSkinValue(val); });
        skinGO.SetActive(true);
        this.gameManager.SkinDatas[skinNumber].Unlocked = true;
        questionMarkGO.SetActive(false);
    }

    void HideSkin()
    {
        toggleButton.interactable = false;
        skinGO.SetActive(false);
        questionMarkGO.SetActive(true);
    }

    public void RightCircleSelected(bool ballVal)
    {
        rightBall = ballVal;
    }

    void SetSkinValue(bool isSelected)
    {
        if (isSelected)
        {
            toggleButton.interactable = false;
            if (!rightBall)
            {
                this.gameManager.LeftBallSkin = this.skinNumber;
            }
            else if (rightBall)
            {
                this.gameManager.RightBallSkin = this.skinNumber;
            }
            skinValueChanged.Raise();
        }
        else if (!isSelected)
        {
            toggleButton.interactable = true;
        }
    }
}

﻿using UnityEngine;
using UnityEngine.UI;

public class CircleSkinChanger : MonoBehaviour
{
    enum Circle
    {
        right,
        left
    };

    [SerializeField] Circle balls;
    [SerializeField] GameManager gameManager;

    void Awake()
    {
        ChangeSkin();
    }

    public void ChangeSkin()
    {
        if (balls == Circle.right)
        {
            var temp = this.gameManager.SkinDatas[this.gameManager.RightBallSkin].SkinSprite;

            if(this.gameObject.GetComponent<SpriteRenderer>())
            {
                this.gameObject.GetComponent<SpriteRenderer>().sprite = temp;
            }
            else
            {
                this.gameObject.GetComponent<Image>().sprite = temp;
            }
        }
        else if (balls == Circle.left)
        {
            var temp = this.gameManager.SkinDatas[this.gameManager.LeftBallSkin].SkinSprite;

            if (this.gameObject.GetComponent<SpriteRenderer>())
            {
                this.gameObject.GetComponent<SpriteRenderer>().sprite = temp;
            }
            else
            {
                this.gameObject.GetComponent<Image>().sprite = temp;
            }
        }
    }
}

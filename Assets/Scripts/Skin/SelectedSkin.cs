﻿using UnityEngine;

public class SelectedSkin : MonoBehaviour
{
    [SerializeField] GameManager gameManager;

    RandomUnlock ru;
    void Start()
    {
        ru = this.gameObject.GetComponent<RandomUnlock>();
        UpdateSelectedSkin(false);
    }

    public void UpdateSelectedSkin(bool rightSkin)
    {
        if (rightSkin)
        {
            ru.skinHolders[this.gameManager.RightBallSkin].GetComponent<SkinHandler>().toggleButton.isOn = true;
        }
        else if (!rightSkin)
        {
            ru.skinHolders[this.gameManager.LeftBallSkin].GetComponent<SkinHandler>().toggleButton.isOn = true;
        }
    }
}

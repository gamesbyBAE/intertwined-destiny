﻿using UnityEngine;

[CreateAssetMenu(fileName = "New Skin", menuName = "Skin", order = 55)]
public class Skin : ScriptableObject
{
    //[SerializeField] int serialNo;
    [SerializeField] Sprite skinSprite;
    [SerializeField] bool unlocked;

    public Sprite SkinSprite { get { return skinSprite; } }
    public bool Unlocked { get { return unlocked; } set { unlocked = value; } }
}

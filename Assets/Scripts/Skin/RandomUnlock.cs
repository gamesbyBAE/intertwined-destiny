﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class RandomUnlock : MonoBehaviour
{
    [SerializeField] GameManager gameManager;
    [SerializeField] GameEvent deductSC;
    [SerializeField] GameEvent unlockedSkin;
    public GameObject[] skinHolders;
    [SerializeField] GameObject outlineBorder;
    [SerializeField] GameObject confetti;
    [SerializeField] float lotteryDuration;

    List<int> lockedIndexArray;
    RectTransform outlineBorderRT;
    IEnumerator coroutine;
    void Start()
    {
        lockedIndexArray = new List<int>();
        outlineBorderRT = outlineBorder.GetComponent<RectTransform>();
        outlineBorder.SetActive(false);
        FillLockedIndex();
    }

    public void UnlockSkin()
    {
        if (this.gameManager.SoftCurrency >= 200)
        {
            deductSC.Raise();
            this.coroutine = LotteryRollAnimation();
            StartCoroutine(this.coroutine);
        }
    }

    IEnumerator LotteryRollAnimation()
    {
        outlineBorder.SetActive(true);
        LeanTween.alpha(outlineBorderRT, 1, 0f);

        int time = 0;
        int index = 1;
        int lastUsedIndex = 0;
        if (lockedIndexArray.Count == 1)
        {
            lotteryDuration = 1;
        }
        while (time < lotteryDuration)
        {
            time += 1;

            index = Random.Range(0, lockedIndexArray.Count);
            if (index == lastUsedIndex)
            {
                var range = Enumerable.Range(0, lockedIndexArray.Count).Where(i => i != lockedIndexArray.Count);
                index = range.ElementAt(Random.Range(0, lockedIndexArray.Count - 1));
            }
            outlineBorderRT.anchoredPosition = skinHolders[lockedIndexArray[index]].GetComponent<RectTransform>().anchoredPosition;
            lastUsedIndex = index;
            yield return new WaitForSeconds(0.3f);
        }
        yield return new WaitForSeconds(0.3f);

        LeanTween.alpha(outlineBorderRT, 0, 0.5f).setOnComplete(() => outlineBorder.SetActive(false));

        confetti.GetComponent<RectTransform>().anchoredPosition = skinHolders[lockedIndexArray[index]].GetComponent<RectTransform>().anchoredPosition;
        confetti.SetActive(true);
        confetti.GetComponent<ConfettiPop>().PopConfetti();
        unlockedSkin.Raise();

        skinHolders[lockedIndexArray[index]].GetComponent<SkinHandler>().ShowSkin();

        FillLockedIndex();
        StopCoroutine(this.coroutine);
    }

    void FillLockedIndex()
    {
        lockedIndexArray.Clear();
        for (int i = 0; i < this.gameManager.SkinDatas.Length; i++)
        {
            if (!this.gameManager.SkinDatas[i].Unlocked)
            {
                lockedIndexArray.Add(i);
            }
        }
    }
}

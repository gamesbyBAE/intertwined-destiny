﻿using UnityEngine;

public class SmallScorePosition : MonoBehaviour
{
    [SerializeField] GameManager gameManager;
    [SerializeField] float horizontalScreenPercentage;

    void Start()
    {
        if(this.gameManager.ScoreType < 3)
        {
            this.gameObject.SetActive(false);
        }
        else
        {
            var screenWidth = Screen.width / 2;
            Debug.Log(Screen.width);
            Debug.Log("Half: " + screenWidth);
            horizontalScreenPercentage /= 100;
            var xPosition = 0f;

            // Left (Default)
            if (this.gameManager.ScoreType == 3)
            {
                xPosition = -(screenWidth - (screenWidth * horizontalScreenPercentage));
            }
            // Middle
            else if (this.gameManager.ScoreType == 4)
            {
                xPosition = 0;
            }
            // Right
            else if (this.gameManager.ScoreType == 5)
            {
                xPosition = screenWidth - (screenWidth * horizontalScreenPercentage);
            }
            RectTransform rt = this.gameObject.GetComponent<RectTransform>();
            rt.anchoredPosition = new Vector2(xPosition, rt.anchoredPosition.y);
        }
    }
}

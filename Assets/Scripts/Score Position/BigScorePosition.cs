﻿using UnityEngine;

public class BigScorePosition : MonoBehaviour
{
    [SerializeField] GameManager gameManager;
    [SerializeField] float verticalScreenPercentage;
    void Start()
    {
        if (this.gameManager.ScoreType > 2)
        {
            this.gameObject.SetActive(false);
        }
        else
        {
            var screenHeight = Screen.height / 2;
            verticalScreenPercentage /= 100;
            var yPosition = 0f;

            // Bottom (Default)
            if (this.gameManager.ScoreType == 0)
            {
                yPosition = -(screenHeight - (screenHeight * verticalScreenPercentage));
            }
            // Middle
            else if (this.gameManager.ScoreType == 1)
            {
                yPosition = 0;
            }
            // Top
            else if (this.gameManager.ScoreType == 2)
            {
                yPosition = screenHeight - (screenHeight * verticalScreenPercentage);
            }
            this.gameObject.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, yPosition);
        }
    }
}

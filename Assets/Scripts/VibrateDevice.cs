﻿using UnityEngine;

public class VibrateDevice : MonoBehaviour
{
    [SerializeField]
    GameManager gameManager;

    public void VibrateMobileDevice()
    {
        if (this.gameManager.VibrateDevice)
        {
            Handheld.Vibrate();
        }
    }
}

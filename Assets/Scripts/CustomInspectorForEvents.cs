﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;

[CustomEditor(typeof(GameEvent))]
public class CustomInspectorForEvents : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        GameEvent gameEvent = (GameEvent)target;
        if (GUILayout.Button("Raise"))
        {
            gameEvent.Raise();
        }
    }
}
#endif

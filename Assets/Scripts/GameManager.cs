﻿using UnityEngine;

[CreateAssetMenu(fileName = "New Game Manager", menuName = "Game Manager", order = 51)]
public class GameManager : ScriptableObject
{
    //[SerializeField] int currentThemeID;
    [SerializeField] int challengeCost;
    [SerializeField] UnlockableData[] unlockableDatas;
    [SerializeField] Skin[] skinDatas;

    public static GameManager instance = null;


    #region Setter Getters
    public int ChallengeCost { get { return challengeCost; } }

    public bool FirstTime { get; set; }

    public int PlayCount { get; set; }

    public bool InfiniteMode { get; set; }

    public bool NextLevel { get; set; }

    public int ScoreValue { get; set; }

    public int HighScore { get; set; }

    public int SoftCurrency { get; set; }

    public int ScoreType { get; set; }

    public int InputType { get; set; }

    public bool Continued { get; set; }

    public bool WatchedAd { get; set; }

    public bool MuteSFX { get; set; }

    public bool MuteMusic { get; set; }

    public bool VibrateDevice { get; set; }

    public Color ObstacleColor { get; set; }

    public Color BackgroundColor { get; set; }

    public int LeftBallSkin { get; set; }

    public int RightBallSkin { get; set; }
    
    public int UsedThemeID { get; set; }

    public int ThemeID { get; set; }

    public int MaxUnlockedTheme { get; set; }

    public int GameOverCount { get; set; }

    public UnlockableData[] UnlockableDatas { get { return unlockableDatas; } set { unlockableDatas = value; } }

    public Skin[] SkinDatas { get { return skinDatas; } set { skinDatas = value; } }
    #endregion

    private void OnEnable()
    {
        ImpData data = EncryptDecryptData.DecryptData();
        if (data == null)
        {
            FirstTime = true;
            SoftCurrency = 50;
            HighScore = 0;
            ScoreType = 0;
            InputType = 1;
            MuteMusic = false;
            MuteSFX = false;
            VibrateDevice = true;
            UsedThemeID = -1;
            LeftBallSkin = 0;
            RightBallSkin = 1;
            ThemeID = 0;
            MaxUnlockedTheme = 0;
            for (int i = 2; i < skinDatas.Length; i++)
            {
                skinDatas[i].Unlocked = false;
            }
        }
        else
        {
            FirstTime = false;
            SoftCurrency = data.xe;
            HighScore = data.nx;
            ScoreType = data.scoreType;
            InputType = data.inputType;
            MuteMusic = data.muteMusic;
            MuteSFX = data.muteSFX;
            LeftBallSkin = data.lbs;
            RightBallSkin = data.rbs;
            VibrateDevice = data.vibrateDevice;
            UsedThemeID = data.usedThemeID;
            ThemeID = data.themeID;
            MaxUnlockedTheme = data.maxUnlocked;
            ObstacleColor = unlockableDatas[data.themeID].ObstacleColor;
            BackgroundColor = unlockableDatas[data.themeID].BackgroundColor;
            for (int i = 0; i < data.xikyx.Length; i++)
            {
                SkinDatas[i].Unlocked = data.xikyx[i];
            }
        }

        PlayCount = 0;
        ScoreValue = 0;
        GameOverCount = 0;
        Continued = false;
        WatchedAd = false;
        NextLevel = false;
    }
}

﻿using UnityEngine;

public class DisableContinueButton : MonoBehaviour
{
    [SerializeField] GameManager gameManager;
    [SerializeField] bool disableOnStartIfWatchedAd;
    void Start()
    {
        if(gameManager.WatchedAd && disableOnStartIfWatchedAd)
        {
            this.gameObject.SetActive(false);
        }
        else
        {
            this.gameObject.SetActive(true);
        }
    }

    public void ContinuedOnce()
    {
        this.gameObject.SetActive(false);
    }

}

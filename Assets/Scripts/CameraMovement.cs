﻿using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    [SerializeField] GameManager gameManager;

    [SerializeField] Transform leftBall, rightBall;
    [SerializeField] float yValdiff;
    [SerializeField] LeanTweenType easeType;
    [SerializeField] float tweenDuration;

    float targetPos;

    void Start()
    {
        this.gameObject.GetComponent<Camera>().backgroundColor = this.gameManager.BackgroundColor;
    }
    void LateUpdate()
    {   
        if (leftBall.position.y > this.transform.position.y + yValdiff)
        {
            targetPos = leftBall.transform.position.y;
            TweenCameraPosition(targetPos);
        }
        else if (rightBall.position.y > this.transform.position.y + yValdiff)
        {
            targetPos = rightBall.transform.position.y;
            TweenCameraPosition(targetPos);
        }
    }

    void TweenCameraPosition(float targetPosition)
    {
        if (LeanTween.isTweening(this.gameObject))
        {
            LeanTween.cancel(this.gameObject);
        }
        LeanTween.moveY(this.gameObject, targetPosition, tweenDuration).setEase(easeType);
    }

    public void StopFollow(bool gameOver)
    {
        LeanTween.cancel(this.gameObject);
        GetComponent<CameraMovement>().enabled = false;
    }
}

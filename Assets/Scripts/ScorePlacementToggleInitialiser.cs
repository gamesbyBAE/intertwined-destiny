﻿using UnityEngine;
using UnityEngine.UI;
public class ScorePlacementToggleInitialiser : MonoBehaviour
{
    [SerializeField] GameManager gameManager;
    [SerializeField] Toggle[] toggleButtons;
    void Awake()
    {
        toggleButtons[this.gameManager.ScoreType].isOn = true;
    }
}

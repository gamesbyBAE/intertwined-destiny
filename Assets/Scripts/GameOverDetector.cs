﻿using UnityEngine;

public class GameOverDetector : MonoBehaviour
{
    [SerializeField] GameEvent onGameOver;

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Kill Zone"))
        {
            //Debug.Log("GAME OVER, kill zone");
            onGameOver.Raise();
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Obstacle") || collision.gameObject.CompareTag("Obstacle Extra Space"))
        {
            //Debug.Log("GAME OVER, obstacle");
            onGameOver.Raise();
        }
    }
}

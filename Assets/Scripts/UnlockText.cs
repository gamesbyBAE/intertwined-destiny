﻿using UnityEngine;
using TMPro;

public class UnlockText : MonoBehaviour
{
    [SerializeField] GameManager gameManager;
    void Start()
    {
        //string textValue = "(" + (this.gameManager.MaxUnlockedTheme + 1).ToString() + "/" + (this.gameManager.UnlockableDatas.Length).ToString() + ")";
        
        /*
         * For demo hard code the value of total obstacles (the denominator). In final build use the above line to dynamically set the value.
        */
        string textValue = "(" + (this.gameManager.MaxUnlockedTheme + 1).ToString() + "/10)";
        this.gameObject.GetComponent<TextMeshProUGUI>().text = textValue;
    }
}

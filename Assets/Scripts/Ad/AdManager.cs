﻿using UnityEngine;

[CreateAssetMenu(fileName = "New Ad Manager",menuName = "Ad Manager", order = 52)]
public class AdManager : ScriptableObject
{
    [SerializeField] string playStoreID;
    [SerializeField] string rewardedPlacementID;
    [SerializeField] string bannerPlacementID;
    [SerializeField] string interstitialPlacementID;
    [SerializeField] bool testMode;
    

    #region Getters
    public string PlayStoreID { get { return playStoreID; } }

    public string RewardedPlacementID { get { return rewardedPlacementID; } }

    public string BannerPlacementID { get { return bannerPlacementID; } }

    public string InterstitialPlacementID { get { return interstitialPlacementID; } }

    public bool TestMode { get { return testMode; } }

    #endregion

}

﻿using System.Collections;
using UnityEngine;
using UnityEngine.Advertisements;

public class RewardedAd1 : MonoBehaviour, IUnityAdsListener
{
    [SerializeField] GameManager gameManager;
    [SerializeField] AdManager adManager;

    [Header("Game Events")]
    [SerializeField] GameEvent muteMusic;
    [SerializeField] GameEvent unmuteMusic;
    [SerializeField] GameEvent muteSFX;
    [SerializeField] GameEvent unmuteSFX;
    [SerializeField] GameEvent watchedAd;
    [SerializeField] GameEvent skippedAd;
    [SerializeField] GameEvent scoreMultiplied;

    bool scoreMultiplier;
    void Start()
    {
        //Advertisement.AddListener(this);
        Advertisement.Initialize(adManager.PlayStoreID, adManager.TestMode);
    }

    public void DisplayAd(bool scoreMultiply)
    {
        scoreMultiplier = scoreMultiply;
        Advertisement.AddListener(this);

        if (!Advertisement.IsReady(this.adManager.RewardedPlacementID))
        {
            Advertisement.Initialize(adManager.PlayStoreID, adManager.TestMode);
            return;
        }
        Advertisement.Show(this.adManager.RewardedPlacementID);
    }

    public void OnUnityAdsDidFinish(string placementId, ShowResult showResult)
    {
        switch (showResult)
        {
            case ShowResult.Finished:
                if (placementId == this.adManager.RewardedPlacementID) //Because also fires when interstitial ads finish
                {
                    UnMuteGameAudio();
                    if (scoreMultiplier)
                    {
                        scoreMultiplied.Raise();
                    }
                    else if (!scoreMultiplier)
                    {
                        watchedAd.Raise();
                    }
                }
                break;

            case ShowResult.Skipped:
                skippedAd.Raise();
                UnMuteGameAudio();
                break;

            case ShowResult.Failed:
                break;
        }
        Advertisement.RemoveListener(this);
    }

    public void OnUnityAdsDidStart(string placementId)
    {
        MuteGameAudio();
    }

    public void OnUnityAdsReady(string placementId)
    {
        //Debug.Log(placementId + " is ready.");
    }

    public void OnUnityAdsDidError(string message)
    {
        Debug.LogError(message);
    }

    void MuteGameAudio()
    {
        if (!this.gameManager.MuteMusic && this.gameManager.MuteSFX)
        {
            muteMusic.Raise();
        }
        else if (!this.gameManager.MuteSFX && this.gameManager.MuteMusic)
        {
            muteSFX.Raise();
        }
        else if (!this.gameManager.MuteMusic && !this.gameManager.MuteSFX)
        {
            muteMusic.Raise();
            muteSFX.Raise();
        }
    }

    void UnMuteGameAudio()
    {
        if (!this.gameManager.MuteMusic && this.gameManager.MuteSFX)
        {
            unmuteMusic.Raise();
        }
        else if (!this.gameManager.MuteSFX && this.gameManager.MuteMusic)
        {
            unmuteSFX.Raise();
        }
        else if (!this.gameManager.MuteMusic && !this.gameManager.MuteSFX)
        {
            unmuteMusic.Raise();
            unmuteSFX.Raise();
        }
    }
}

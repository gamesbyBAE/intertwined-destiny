﻿using System.Collections;
using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.SceneManagement;

public class BannerAd : MonoBehaviour
{
    [SerializeField] GameManager gameManager;
    [SerializeField] AdManager adManager;
    void Start()
    {
        if ( Advertisement.Banner.isLoaded || Advertisement.IsReady(this.adManager.BannerPlacementID))
        {
            DisplayBannerAd();
        }
        else
        {
            Advertisement.Initialize(this.adManager.PlayStoreID, this.adManager.TestMode);
            StartCoroutine(ShowBannerWhenReady());
        }
    }

    IEnumerator ShowBannerWhenReady()
    {
        while (!Advertisement.IsReady(this.adManager.BannerPlacementID))
        {
            yield return new WaitForSeconds(0.5f);
        }
        DisplayBannerAd();
        StopCoroutine(ShowBannerWhenReady());
    }

    private void OnDestroy()
    {
        Advertisement.Banner.Hide();
    }

    void DisplayBannerAd()
    {
        if (!this.gameManager.FirstTime || SceneManager.GetActiveScene().buildIndex == 0)
        {
            Advertisement.Banner.SetPosition(BannerPosition.BOTTOM_CENTER);
            Advertisement.Banner.Show(this.adManager.BannerPlacementID);
        }
        else
        {
            Advertisement.Banner.Hide();
        }
    }
}

﻿using UnityEngine;
using UnityEngine.Advertisements;

public class InterstitialAd : MonoBehaviour, IUnityAdsListener
{
    [SerializeField] GameManager gameManager;
    [SerializeField] AdManager adManager;
    [SerializeField] int gameOverCountLimit;
    
    [Header("Game Events")]
    [SerializeField] GameEvent muteMusic;
    [SerializeField] GameEvent unmuteMusic;
    [SerializeField] GameEvent muteSFX;
    [SerializeField] GameEvent unmuteSFX;

    void Start()
    {
        Advertisement.Initialize(this.adManager.PlayStoreID, this.adManager.TestMode);
    }
    
    public void DisplayInterstitialAd()
    {
        this.gameManager.GameOverCount += 1;
        if (this.gameManager.GameOverCount >= gameOverCountLimit)
        {
            Advertisement.AddListener(this);
            if (!Advertisement.IsReady(this.adManager.RewardedPlacementID))
            {
                Advertisement.Initialize(adManager.PlayStoreID, adManager.TestMode);
                return;
            }
            Advertisement.Show(this.adManager.InterstitialPlacementID);
        }
    }

    public void OnUnityAdsDidFinish(string placementId, ShowResult showResult)
    {
        switch (showResult)
        {
            case ShowResult.Failed:
                UnMuteGameAudio();
                break;

            case ShowResult.Skipped:
                UnMuteGameAudio();
                break;

            case ShowResult.Finished:
                if (placementId == this.adManager.InterstitialPlacementID)
                {
                    UnMuteGameAudio();
                    this.gameManager.GameOverCount = 0;
                }
                break;
        }
        Advertisement.RemoveListener(this);
    }

    public void OnUnityAdsDidError(string message)
    {
        throw new System.NotImplementedException();
    }


    public void OnUnityAdsDidStart(string placementId)
    {
        MuteGameAudio();
    }

    public void OnUnityAdsReady(string placementId)
    {
        throw new System.NotImplementedException();
    }

    void MuteGameAudio()
    {
        if (!this.gameManager.MuteMusic && this.gameManager.MuteSFX)
        {
            muteMusic.Raise();
        }
        else if (!this.gameManager.MuteSFX && this.gameManager.MuteMusic)
        {
            muteSFX.Raise();
        }
        else if (!this.gameManager.MuteMusic && !this.gameManager.MuteSFX)
        {
            muteMusic.Raise();
            muteSFX.Raise();
        }
    }

    void UnMuteGameAudio()
    {
        if (!this.gameManager.MuteMusic && this.gameManager.MuteSFX)
        {
            unmuteMusic.Raise();
        }
        else if (!this.gameManager.MuteSFX && this.gameManager.MuteMusic)
        {
            unmuteSFX.Raise();
        }
        else if (!this.gameManager.MuteMusic && !this.gameManager.MuteSFX)
        {
            unmuteMusic.Raise();
            unmuteSFX.Raise();
        }
    }
}

﻿using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public static class EncryptDecryptData
{
    static readonly string fileName = "cmon.bae";
    public static void EncryptData(GameManager gameManager)
    {
        BinaryFormatter bf = new BinaryFormatter();
        string path = Path.Combine(Application.persistentDataPath, fileName);
        #if UNITY_EDITOR
            Debug.Log(path);
        #endif
        FileStream stream = new FileStream(path, FileMode.Create);
        ImpData data = new ImpData(gameManager);
        bf.Serialize(stream, data);
        stream.Close();
    }

    public static ImpData DecryptData()
    {
        string path = Path.Combine(Application.persistentDataPath, fileName);
        if (File.Exists(path))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);
            ImpData data = bf.Deserialize(stream) as ImpData;
            stream.Close();
            return data;
        }
        else
        {
            return null;
        }
    }
}

[System.Serializable]
public class ImpData
{
    public int nx, xe, themeID, usedThemeID, maxUnlocked, scoreType, inputType, rbs, lbs;
    public bool muteSFX, muteMusic, vibrateDevice;
    public bool[] xikyx;
    public float ballColorR, ballColorG, ballColorB;
    public float backgroundColorR, backgroundColorG, backgroundColorB;
    public ImpData(GameManager gameManager)
    {
        xe = gameManager.SoftCurrency;
        nx = gameManager.HighScore;
        scoreType = gameManager.ScoreType;
        inputType = gameManager.InputType;

        muteSFX = gameManager.MuteSFX;
        muteMusic = gameManager.MuteMusic;
        vibrateDevice = gameManager.VibrateDevice;

        themeID = gameManager.ThemeID;
        usedThemeID = gameManager.UsedThemeID;
        maxUnlocked = gameManager.MaxUnlockedTheme;

        rbs = gameManager.RightBallSkin;
        lbs = gameManager.LeftBallSkin;

        ballColorR = gameManager.ObstacleColor.r;
        ballColorG = gameManager.ObstacleColor.g;
        ballColorB = gameManager.ObstacleColor.b;

        backgroundColorR = gameManager.BackgroundColor.r;
        backgroundColorG = gameManager.BackgroundColor.g;
        backgroundColorB = gameManager.BackgroundColor.b;

        xikyx = new bool[gameManager.SkinDatas.Length];
        for (int i = 0; i < xikyx.Length; i++)
        {
            xikyx[i] = gameManager.SkinDatas[i].Unlocked;
        }
    }
}

﻿using UnityEngine;

public class DisableNextButton : MonoBehaviour
{
    [SerializeField] GameManager gameManager;

    private void OnEnable()
    {
        if (this.gameManager.ThemeID == 0)
        {
            this.gameObject.SetActive(false);
        }   
    }
}

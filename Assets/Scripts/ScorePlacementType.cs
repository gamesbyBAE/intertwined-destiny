﻿using UnityEngine;

public class ScorePlacementType : MonoBehaviour
{
    [SerializeField] GameManager gameManager;

    public void SetScoreType(int i)
    {
        this.gameManager.ScoreType = i;
    }
}
